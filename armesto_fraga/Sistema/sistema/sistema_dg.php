<style>

body, html {
    /*height: 100%;*/
    background: linear-gradient( rgb(0, 0, 0), rgb(97, 12, 12));
}

.card-container.card {
	max-width: 1200px;
	padding: left 40px 40px;
	float: left;
}

.div{
  margin: auto;
  float: left;
}



.card {
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 25px 30px;
    margin: 0 auto 25px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.boton_personalizado{
    text-decoration: none;
    padding: 0px;
    font-weight: 0px;
    font-size: 44px;
    color: #ffffff;
    background-color: #1883ba;
    border-radius: 0px;
    border: 1px solid #0016b0;
}
.boton_personalizado:hover{
    color: #1883ba;
    background-color: #ffffff;
}




table.comicGreen {
  font-family: "Calibri", cursive, sans-serif;
  border: 2px solid #784949;
  background-color: #e1e1e1;
  /*width: 100%;*/
  text-align: left;
  border-collapse: collapse;
}
table.comicGreen td, table.comicGreen th {
  border: 1px solid #784949;
  padding: 3px 2px;
}
table.comicGreen tbody td {
  font-size: 19px;
  font-weight: bold;
  color: #784949;
}
table.comicGreen tr:nth-child(even) {
  background: #e0cccc;
}
table.comicGreen tfoot {
  font-size: 21px;
  font-weight: bold;
  color: #FFFFFF;
  background: #784949;
  background: -moz-linear-gradient(top, #9a7676 0%, #855b5b 66%, #784949 100%);
  background: -webkit-linear-gradient(top, #9a7676 0%, #855b5b 66%, #784949 100%);
  background: linear-gradient(to bottom, #9a7676 0%, #855b5b 66%, #784949 100%);
  border-top: 1px solid #444444;
}
table.comicGreen tfoot td {
  font-size: 21px;
}


/****************************/
body {
  margin: 0;
  padding: 0;
  font-family: sans-serif;
}

h2 {
  font-size: 1.1em;
  margin-top: 2em;
  text-align: center;
}

main {
  width: 80%;
  margin: auto;
}

.modal {
  background: rgba(0, 0, 0, 0.9);
  color: #fff;
  position: fixed;
  top: -100vh;
  left: 0;
  height: 100vh;
  width: 100vw;
  transition: all .5s;
}
.modal div {
  width: 60%;
  height: 40%;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin: auto;
  font-size: 1.5em;
  text-align: center;
}

.mostrar-modal {
  display: none;
}
.mostrar-modal + label {
  background: steelblue;
  display: table;
  margin: auto;
  color: #fff;
  line-height: 3;
  padding: 0 1em;
  text-transform: uppercase;
  cursor: pointer;
}
.mostrar-modal + label:hover {
  background: #38678f;
}
.mostrar-modal:checked ~ .modal {
  top: 0;
}
.mostrar-modal:checked ~ .cerrar-modal + label {
  display: block;
}
.cerrar-modal {
  display: none;
}
.cerrar-modal + label {
  position: absolute;
  top: 1em;
  right: 1em;
  z-index: 100;
  color: #fff;
  font-weight: bold;
  cursor: pointer;
  background: tomato;
  width: 25px;
  height: 25px;
  line-height: 25px;
  text-align: center;
  border-radius: 50%;
  display: none;
  transition: all .5s;
}
.cerrar-modal:checked ~ .modal {
  top: -100vh;
}
.cerrar-modal:checked + label {
  display: none;
}/**************************/



</style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
        $(".btn").click(function() {
          id = "tr_" + $(this).attr("id").split("_")[1];
          $("." + id).toggle();
        });
        $(".boton_personalizado").click(function() {
            if($(this).attr("id") == "mostrar_mesa") {
              $.ajax({
                method: "POST",
                url: "../mesa/modificar/mesas_oc.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            } else if($(this).attr("id") == "buscar_mesa") {
              $.ajax({
                method: "POST",
                url: "../mesa/buscar/buscarXmesa.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "cargar_mesa") {
              $.ajax({
                method: "POST",
                url: "../mesa/cargado/cargar_mesa.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "mod_mesa1") {
              $.ajax({
                method: "POST",
                url: "../mesa/modificar/ocu_deso.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "mostrar_clientes"){
              $.ajax({
                method: "POST",
                url: "../cliente/modificar/mostrar_clientes.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "buscaportelefono"){
              $.ajax({
                method: "POST",
                url: "../cliente/buscar/buscarXtelefono.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            } 
            else if($(this).attr("id") == "carga_cliente") {
              $.ajax({
                method: "POST",
                url: "../cliente/cargado/clientes.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "modificar_cliente"){
              $.ajax({
                method: "POST",
                url: "../cliente/modificar/modificar_cliente.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "bajar_cliente"){
              $.ajax({
                method: "POST",
                url: "../cliente/baja/bajar_cliente.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "mostrar_productos"){
              $.ajax({
                method: "POST",
                url: "../productos/modificar/mostrar_productos.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "buscarproducto"){
              $.ajax({
                method: "POST",
                url: "../productos/buscar/buscarproducto.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "cargar_producto"){
              $.ajax({
                method: "POST",
                url: "../productos/cargado/producto.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "modificar_nombre_producto"){
              $.ajax({
                method: "POST",
                url: "../productos/modificar/modificar_nombre_producto.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "modificar_precio_producto"){
              $.ajax({
                method: "POST",
                url: "../productos/modificar/modificar_precio_producto.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
            else if($(this).attr("id") == "bajar_producto"){
              $.ajax({
                method: "POST",
                url: "../productos/baja/bajar_producto.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }      
            else if($(this).attr("id") == "crear_usuario") {
              $.ajax({
                method: "POST",
                url: "../usuario/cargado/usuarios.php",
                dataType: "html",
                success: function(result){                
                  $("#contenedor").html(result);                
                }
              });
            }
        });
    });
  </script>


<table ">
<tr>
  <td> 
<table class="comicGreen">
  <tfoot>
    <tr>
      <td>Sistema</td>
      <td>"San Camilo"</td>
      <td>2.0</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td></td>
      <td class="btn" id="bnt_mesas">Mesas</td>
      <td></td>
    </tr>


    <tr class="tr_mesas" style="display: none;">

      <td><a href="#" id="mostrar_mesa" class="boton_personalizado" border=0><img src="../iconos/png/notepad.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Mostrar mesas</td>

      <td><a href="#" id="cargar_mesa" class="boton_personalizado" border=0><img src="../iconos/mas.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Cargar mesa </td>

      <td><a href="#" id="buscar_mesa" class="boton_personalizado" border=0><img src="../iconos/png/search.png" width="40px" height="40px" alt="buscar por mesa"></a> Buscar mesa</td>

    </tr>

    <tr class="tr_mesas" style="display: none;">

      <td></td>

      <td><a href="#" id="mod_mesa1" class="boton_personalizado" border=0><img src="../iconos/png/notepad-2.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Cambiar producto en mesa </td>

      <td></td>
      
    </tr>

    <tr>
      <td></td>
      <td class="btn" id="bnt_cliente">Clientes</td>
      <td></td>
    </tr>

    <tr class="tr_cliente" style="display: none;">

      <td><a href="#" id="mostrar_clientes" class="boton_personalizado" border=0><img src="../iconos/png/notepad.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Mostrar clientes</td>

      <td><a href="#" id="buscaportelefono" class="boton_personalizado" border=0><img src="../iconos/png/search.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Buscar Cliente por telefono</td>

      <td><a href="#" id="carga_cliente" class="boton_personalizado" border=0><img src="../iconos/mas.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Agregar cliente</td>

    </tr>

    <tr class="tr_cliente" style="display: none;">

      <td><a href="#" id="modificar_cliente" class="boton_personalizado" border=0><img src="../iconos/png/notepad-2.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Modificar cliente</td>

      <td><a href="#" id="bajar_cliente" class="boton_personalizado" border=0><img src="../iconos/basura.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Eliminar cliente</td>

    <td></td>
    </tr>

      <tr>
        <td></td>
        <td class="btn" id="bnt_producto">Productos</td>
        <td></td>
      </tr>

      <tr class="tr_producto" style="display: none;">

        <td><a href="#" id="mostrar_productos" class="boton_personalizado" border=0><img src="../iconos/png/notepad.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Mostrar Productos</td>

        <td><a href="#" id="buscarproducto" class="boton_personalizado" border=0><img src="../iconos/png/search.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Buscar Producto</td>

        <td><a href="#" id="cargar_producto" class="boton_personalizado" border=0><img src="../iconos/mas.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Agregar Producto</td>

      </tr>

      <tr class="tr_producto" style="display: none;">

        <td><a href="#" id="modificar_nombre_producto" class="boton_personalizado" border=0><img src="../iconos/png/notepad-2.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Modificar nombre de producto</td>

        <td><a href="#" id="modificar_precio_producto" class="boton_personalizado" border=0><img src="../iconos/png/notepad-2.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Modificar precio de producto</td>

        <td><a href="#" id="bajar_producto" class="boton_personalizado" border=0><img src="../iconos/basura.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Eliminar Producto</td>

        <td></td>
      </tr>

    <tr>
      <td></td>
      <td class="btn" id="bnt_usuario">Usuario</td>
      <td></td>
    </tr>

    <tr class="tr_usuario" style="display: none;">
      <td></td>
      <td><a href="#" id="crear_usuario" class="boton_personalizado" border=0><img src="../iconos/png/user-4.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Crear nuevo usuario</td>
      <td></td>
    </tr>

    <tr>  
      <td></td>
      <td><a href="../loguear_con_diseno.php.html" id="" border=0><img src="../iconos/png/incoming.png" width="40px" height="40px" alt="crear nuevo usuario"></a> Volver</td>
      <td></td>

    </tr>
  </tbody>
</table>

<div>

  </td>
    <td id="contenedor" style="background-color: #e1e1e1">
      <td id="contenedor1" style="background-color: #e1e1e1">
      
  </td>
<tr>