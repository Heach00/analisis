<html>

<style>
body{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

 td,th {
    border: 1px solid #ddd;
    padding: 8px;
}

.desocup{
	border: 1px solid #0f0;
    padding: 8px;
}

.ocup{
	border: 1px solid #F00;
    padding: 8px;
}


tr:nth-child(even){background-color: #f2f2f2;}

tr:hover {background-color: #ddd;}

#th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
.boton_personalizado{
    text-decoration: none;
    padding: 4px;
    font-weight: 80;
    font-size: 14px;
    color: #ffffff;
    background-color: #1883ba;
    border-radius: 2px;
    border: 2px solid #0016b0;
  }
  .boton_personalizado:hover{
    color: #1883ba;
    background-color: #ffffff;

</style>
<body>

<?php
error_reporting(0);
include("../../conectado.php");

$sql= "SELECT pm.id, p.nombre, p.precio, pm.id_mesa from producto_mesa pm, productos p where pm.id_prod = p.id AND pm.estado = 'abierta' order by pm.id_mesa";
$consulta=mysqli_query($conexion,$sql);


while($registro=mysqli_fetch_assoc($consulta)){
    $arr[$registro['id_mesa']] += $registro['precio']; 
}

echo "<table>";
echo "<th>Mesa</th><th>Total</th>";
foreach ($arr as $key => $value) {
	echo "<tr>";
	
		echo "<td>".$key."</td>";

		echo "<td>".$value."</td>";
		
		
	echo "</tr>";
	
}
echo "</table>";


?>
<p>
</body>
</html>
