<html>

<style>
body{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

 td,th {
    border: 1px solid #ddd;
    padding: 8px;
}
.ocup{
	border: 1px solid #f00;
    padding: 8px;
}

.desocup{
	border: 1px solid green;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2;}

tr:hover {background-color: #ddd;}

#th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
<body>

<?php
error_reporting(0);
include("../../conectado.php");


$sql = "SELECT pm.id_prod, pm.id, p.nombre, p.precio, pm.id_mesa from producto_mesa pm, productos p where id_mesa = " .$_POST['id'].  " AND pm.id_prod = p.id AND pm.estado = 'abierta' order by pm.id_mesa";

$resultado=mysqli_query($conexion,$sql);

echo "<table>";
echo "<th>Eliminar producto</th><th>Codigo</th><th>Nombre</th><th>Precio</th>";

$total = 0;
while($registro=mysqli_fetch_assoc($resultado)) 
{
	echo "<tr id='producto_". $registro['id'] . "'>";
	echo "<td><input type='button' class='borrar_producto' id='borrar_producto_" . $registro['id'] . "' value='Borrar'></td>";
    
    echo "<td>".$registro['id_prod']."</td>";
	
	echo "<td>".$registro['nombre']."</td>";
	
	echo "<td>".$registro['precio']."</td>";
	echo "</tr>";
	$total += $registro['precio'];
	
}
echo "</table>";
echo "<b>Total</b> ".$total;

echo "<input class='cerrar_mesa' type='button' id='" . $_POST['id'] . "' value='Cerrar mesa'>";

?>
</body>
</html>
