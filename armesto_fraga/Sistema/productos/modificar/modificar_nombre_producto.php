<html>
<style type="text/css">
  .boton_personalizado{
    text-decoration: none;
    padding: 4px;
    font-weight: 80;
    font-size: 14px;
    color: #ffffff;
    background-color: #1883ba;
    border-radius: 2px;
    border: 2px solid #0016b0;
  }
  .boton_personalizado:hover{
    color: #1883ba;
    background-color: #ffffff;
  }
</style>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
        $("#modnombre").click(function() {
            $.ajax({
              method: "POST",
              url: "../productos/modificar/selectorproducto.php",
              data: {id_prod_mod: $("#id_prod_mod").val(),nombre: $("#nombre").val()},
              dataType: "html",
              success: function(result){                
                $("#contenedor1").html(result);                
              }
            });
        });
    });
  </script>
<b>ID de producto a modificar:</b>
<br>
<input type="text" id="id_prod_mod" name="id_prod_mod">
<br>
<b>Nuevo nombre de producto:</b>
<br>
<input type="text" id="nombre" name="nombre">
<br>
<input type="button" value=enviar id="modnombre" class="boton_personalizado">
</body>
</html>

